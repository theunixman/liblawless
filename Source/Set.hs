module Set (
    module Data.Set.Lens,
    module Data.Set.Unicode,
    Set,
    sing
    ) where

import Control.Lens.Getter
import Data.Set.Lens
import Data.Set.Unicode
import Data.Set (Set, singleton)

sing ∷ Getter a (Set a)
sing = to singleton
