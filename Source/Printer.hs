module Printer
    (
     module Text.Printer,
     module Text.Printer.Integral,
     module Text.Printer.Fractional
    ) where


import Text.Printer
import Text.Printer.Integral
import Text.Printer.Fractional
