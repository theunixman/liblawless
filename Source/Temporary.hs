{-|
Module:             Temporary
Description:        Temporary file handling.
Copyright:          © 2016 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Temporary -- (withTempHandle)
where

import Lawless hiding ((<.>))
import IO
import Exception

-- -- | Run a function with a temporary file handle named after the
-- -- passed name. Ensures the handle is unbuffered and in binary mode.
-- withTempHandle ∷ (MonadIO m, MonadMask m) ⇒ RelFile → (Handle → m a) → m a
-- withTempHandle name f =
--     let
--         n = name <> "XXXXXXXXXXXXXXXX"
--     in do
--         td ← getTemporaryDirectory
--         openBinaryTempFile td n $
--             $ \_ h → do
--             -- Make sure the handle is strictly binary with no buffering.
--             liftIO (hSetBuffering h NoBuffering)
--             f h
