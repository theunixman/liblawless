module Text (
     module Data.Text,
     module Data.Text.Lens,
     module Data.String,
     module Text.Read
     ) where

import Data.Text (Text, null, empty)
import Data.Text.Lens
import Data.String (IsString(..))
import Text.Read (Read(..))
