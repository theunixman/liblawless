{-|
Module:             Unicode
Description:        Unicode symbols for Lawless library.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Unicode (
    module Prelude.Unicode,
    module Control.Applicative.Unicode,
    module UNI,
    ) where

import Control.Monad.Unicode as UNI
import Control.Applicative.Unicode ((⊛))
import Data.Bool.Unicode as UNI
import Data.Eq.Unicode as UNI
import Data.Function.Unicode as UNI
import Data.Monoid.Unicode as UNI
import Data.Ord.Unicode as UNI
import Prelude.Unicode ((⋅))
