module Boomerang (
    module Text.Boomerang,
    module Text.Boomerang.TH,
    module Text.Boomerang.String,
    module Control.Category,
    module Text,
    TextBoomerang,
    parseText,
    unparseText,
    (∘)
    ) where

import Lawless hiding ((.), (∘), (<>))
import Text (Text, packed, unpacked, IsText)
import Text.Boomerang
import Text.Boomerang.TH
import Text.Boomerang.String
import Control.Category ((.), id, Category)

type TextError = StringError
type TextBoomerang a b = Boomerang TextError Text a b

(∘) ∷ ∀ (b ∷ k) (c ∷ k) (a ∷ k) (cat ∷ k → k → *).
    Category cat ⇒ cat b c → cat a b → cat a c
(∘) = (.)
infixr 9 ∘

parseText ∷ ∀ r b. (IsText b) ⇒ StringBoomerang () (r :- ()) → b → Either TextError r
parseText p t = parseString p (t ^. unpacked)

unparseText ∷ ∀ r b. (IsText b) ⇒ StringBoomerang () (r :- ()) → r → Maybe b
unparseText p r = view packed <$> unparseString p r
