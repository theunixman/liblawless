{-# Language TemplateHaskell #-}

{-|
Module:             STM.Operator
Description:        An 'Operator' sends and receives messages from a 'Requestor'.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module STM.Operator (
    module STM.Operator.Types,
    module STM.Operator.Operator,
    module STM.Operator.Requestor
    ) where

import STM.Operator.Types
import STM.Operator.Operator
import STM.Operator.Requestor
