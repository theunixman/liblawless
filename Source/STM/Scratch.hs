import Control.Concurrent.STM (STM)
import qualified Control.Concurrent.STM as STM
import Control.Concurrent.STM.TBChan
import Control.Concurrent.STM.TChan
import Control.Concurrent.STM.TSem
import Control.Concurrent.STM.TVar
import Control.Concurrent.STM.TMVar
import Data.Word



-- * Broadcast Channels

-- ** 'Scatter's have a single source of messages that other entities
-- can receive messages from.

-- | A 'Scatter' is a single source that can broadcast messages
-- to any number of subscribers.
newtype Scatter a = Scatter (TChan a)

-- | Creates a new 'Scatter'.
scatter ∷ MonadIO m ⇒ m (Scatter a)
scatter = Scatter <$> atomically newTChan

-- | A 'Gather' receives copies of all messages posted to the
-- 'Scatter' it was derived from.
newtype Gather a = Gather (TChan a)

-- | Create a new 'Gather' from a given 'Scatter'.
gather ∷ MonadIO m ⇒ Scatter a → m (Gather a)
gather (Scatter s) = Gather <$> atomically (dupTChan s)

-- | Post a message to a 'Scatter' to all the subscribed 'Gather's.
scatterMsg ∷ MonadIO m ⇒ Scatter a → a → m ()
scatterMsg (Scatter c) = atomically ∘ writeTChan c

-- | Read a message posted to a 'Gather' by a 'Scatter'.
gatherMsg ∷ MonadIO m ⇒ Gather a → m a
gatherMsg (Gather c) = atomically $ readTChan c

-- ** 'Hub's are a bounded single reader/multiple writer structure.

-- | The length of a 'Hub', guaranteed to be positive.
newtype HubLength = HubLength Word64
    deriving (Eq, Ord, Show, Bounded, Enum, Num, Integral, Real)

-- | The 'Hub' receives the messages from various 'Source's.
newtype Hub a = Hub {unHub ∷ TBChan a}

hub ∷ MonadIO m ⇒ HubLength → m (Hub a)
hub l = liftIO $ Hub <$> (newTBChanIO $ fromIntegral l)

hubMsg ∷ MonadIO m ⇒ (Hub a) → (m a)
hubMsg = atomically ∘ readTBChan ∘ unHub

hubEmpty ∷ MonadIO m ⇒ (Hub a) → (m Bool)
hubEmpty = atomically ∘  isEmptyTBChan ∘ unHub

-- | A 'Source' attached to a 'Hub' that can send messages to it.
newtype Source a = Source {unSource ∷ TBChan a}

-- | Create a 'Source' attached to a 'Hub'.
source ∷ Getter (Hub a) (Source a)
source = to $ Source ∘ unHub

-- | Send a message to a 'Hub' through a 'Source'.
sourceMsg ∷ MonadIO m ⇒ Source a → a → m ()
sourceMsg s = atomically ∘ writeTBChan (unSource s)

-- ** An 'Operator' is a bidirectional channel. It can receive
-- requests from a 'Requestor', and will return the response over the
-- handle given to the 'Requestor'.

-- *** Supporting Types

-- | The length of an 'Operator' queue, guaranteed to be positive.
newtype RequestLength = RequestLength Word64
    deriving (Eq, Ord, Show, Bounded, Enum, Num, Integral, Real)

-- | Current 'Requestor' identifier.
newtype RequestorId = RequestorId Word64
    deriving (Eq, Ord, Show, Bounded, Enum, Num, Integral, Real)

-- | A wrapper for a request that also has the RequestorId associated
-- with it.
data Request q r = Request {
    -- | The 'RequestorId' that generated this 'Request'.
    _qRequestorId ∷ RequestorId,
    _qRequest ∷ q
    }
makeLenses ''Request

-- | A wrapper for a response that also has the original 'Request'.
-- with it.
data Response q r = Response {
    -- | The 'Request' that generated this 'Response'.
    _rRequest ∷ Request q r,
    _rResponse ∷ r
    }
makeLenses ''Response

-- | The incoming requests from the current 'Requestor'.
newtype OperatorRequests q r = OperatorRequests { unOperatorRequests ∷ TBChan (Request q r) }

-- | The outgoing responses to requests from the current 'Requestor'.
newtype OperatorResponses q r = OperatorResponses { unOperatorResponses ∷ TBChan (Response q r) }

-- *** The 'Operator' itself.

-- | The listener for commands. It can only have a single 'Requestor'
-- at any given time.
data Operator q r = Operator {
    -- | Queue length
    _opQueueLength ∷ RequestLength,

    -- | Identifier for the current Requestor. If this is different
    -- than before running a request, the request is no longer
    -- valid. We go back to waiting for the next request from the new
    -- Requestor.  and we dump it.
    _opRequestorId ∷ TVar RequestorId,

    -- | The 'OperatorRequests' receiving messages from the current 'Requestor'.
    _opRequests ∷ TMVar (OperatorRequests q r),

    -- | The 'OperatorResponses' for the current 'Requestor'. All
    -- responses from requests received via '_opRequests' are returned
    -- here.
    _opResponses ∷ TMVar (OperatorResponses q r)
    }

-- | Returns the 'RequestLength' used for the 'OperatorRequests' and
-- 'OperatorResponses' queues.
opQueueLength ∷ Getter (Operator q r) RequestLength
opQueueLength = to _opQueueLength

-- | Manipulates the current 'RequestorId' of the 'Operator'.
opRequestorId ∷ Lens (Operator q r) (Maybe )

-- | Create a new 'Operator' with no 'Requestor'.
operator ∷ ∀ (m ∷ * → *) q r. (MonadIO m) ⇒ RequestLength → m (Operator q r)
operator l =
    liftIO $ Operator l
    <$> newTVarIO 0
    ⊛ newEmptyTMVarIO
    ⊛ newEmptyTMVarIO

-- | Check if a 'Request' is valid by checking that the 'Request'
-- 'qRequestorId' matches the current 'opRequestorId'. If it's
-- valid, returns 'Just' 'Request'. Otherwise returns 'Nothing'.
--
-- This happens in 'STM' so that it can be used both before processing
-- and after processing, since we can't process 'Request's in STM.
validRequest ∷ Operator q r → Request q r → STM (Maybe (Request q r))
validRequest oper req =
    bool Nothing (Just req) ∘ (≡ (req ^. qRequestorId)) <$> readTVar (oper ^. opRequestorId)

-- | Receive a 'Request' from a 'Requestor', check if it's valid, and
-- if it is, return it.
readRequest ∷ (MonadIO m) ⇒ Operator q r → m (Maybe (Request q r))
readRequest op = atomically $ do
    readTMVar (op ^. opRequests) ≫= readTBChan ∘ unOperatorRequests ≫= validRequest op

-- | Create and send a response if the 'RequestorId' matches the
-- current 'RequestorId' and the Requestor is still connected. If so,
-- return 'Just' 'Response'. Otherwise, return 'Nothing'.
sendResponse ∷ (MonadIO m) ⇒ Operator q r → Request q r → r → m (Maybe r)
sendResponse oper req resp =atomically $
    validRequest oper req
    ≫= writeTBChan ∘ unOperatorResponses (oper ^. opResponses) ≫ resp

-- | Reads a 'Request', calls @f@ with it, and only returns the
-- response if the 'RequestorId' hasn't changed.
runRequest ∷ (MonadIO m) ⇒ Operator q r → (q → m r) → m (Maybe r)
runRequest op f = do
    readRequest op ≫= return ∘ f (q ^. qRequest) ≫= sendResponse op q

-- *** The 'Requestor'

-- | Each 'Requestor' sends 'Request's to an 'Operator'. When a
-- 'Requestor' is done, it disconnects from the 'Operator'.
data Requestor q r = Requestor {
    _reqRequestorId ∷ RequestorId,
    _reqOperator ∷ Operator q r,
    _reqResponses ∷ OperatorResponses q r
    }
makeLenses ''Requestor

-- | Create a 'Requestor' from an 'Operator' as soon as there's an
-- opening. Increments the RequestorId so we know it's a new one.
requestor ∷ (MonadIO m) ⇒ Getter (Operator q r) (m (Requestor q r))
requestor =
          let
              qLen = oper ^. opQueueLength
              qQ = OperatorRequests <$> newTBChanIO qLen
              qR = OperatorResponses <$> newTBChanIO qlen
          in to $ do
              q ← qQ
              r ← qR
              atomically $ do
                  putTMVar (oper ^. opRequests) q
                  putTMVar (oper ^. opResponses) r
                  modifyTVar (+1) (oper ^. opRequestorId)
                  Requestor op <$> readTVar (oper ^. opRequestorId) ⊛ pure q ⊛ pure r

-- | Remove the current requestor if it's still connected. Invalidates
-- the RequestorId by incrementing it.
unRequestor ∷ (MonadIO m) ⇒ (Requestor q r) → m ()
unRequestor r =
    let
        op = r ^. reqOperator
        rid = r ^. qRequestorId
    in atomically $ do
        orid ← readMVar (op ^. opRequestorId)
        unless (rid ≡ orid) $ do
            tryTakeTMVar $ op ^. opResponses
            tryTakeTMVar $ op ^. opRequests
            modifyTVar (+1) (op ^. opRequestorId)

-- | Run a function with a 'Requestor' and clean up after.
withRequestor ∷ (MonadIO m) ⇒ Operator q r → (f → (Requestor q r) → m a) → m a
withRequestor f = op ^. requestor ≫= finally (unRequestor op) ∘ f

-- | Send a request and wait for a response.
request ∷ (MonadIO m) ⇒ Requestor q r → q → m (Maybe r)
request req q =
    let
        op = req ^. reqOperator
    in atomically $ do
        rid ← tryReadMVar (op ^. opRequestorId)
        unlessM ((≡ (Just $ req ^. qRequestorId)) <$> (tryReadMVar (op ^. opRequestorId))) $ do
            (unOperatorRequests <$> tryReadTMVar (op ^. opRequests)) ≫=
                flip writeTBChan ∘ Request (req ^. qRequestorId) q ≫
                tryReadTMVar (req ^. reqOperator ) ≫= readTBChan ≫= view rResponse
