{-|
Module:             STM.Operator.Requestor
Description:        The client of an 'Operator'.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module STM.Operator.Requestor (
    Requestor,
    withRequestor,
    request
    ) where

import STM.Operator.Types

-- * The 'Requestor'

-- | Each 'Requestor' sends 'Request's to an 'Operator'. When a
-- 'Requestor' is done, it disconnects from the 'Operator'.
data Requestor q r = Requestor {
    _reqRequestorId ∷ RequestorId,
    _reqOperator ∷ Operator q r,
    _reqResponses ∷ OperatorResponses q r
    }
makeLenses ''Requestor

-- | Create a 'Requestor' from an 'Operator' as soon as there's an
-- opening. Increments the RequestorId so we know it's a new one.
requestor ∷ (MonadBase IO m) ⇒ Getter (Operator q r) (m (Requestor q r))
requestor =
          let
              qLen = oper ^. opQueueLength
              qQ = OperatorRequests <$> newTBChanIO qLen
              qR = OperatorResponses <$> newTBChanIO qlen
          in to $ do
              q ← qQ
              r ← qR
              atomically $ do
                  putTMVar (oper ^. opRequests) q
                  putTMVar (oper ^. opResponses) r
                  opRequestorIdInc oper
                  Requestor op <$> readTVar (oper ^. opRequestorId) ⊛ pure q ⊛ pure r

-- | Remove the current requestor if it's still connected. Invalidates
-- the RequestorId by incrementing it.
unRequestor ∷ (MonadBase IO m) ⇒ (Requestor q r) → m ()
unRequestor r =
    let
        op = r ^. reqOperator
        rid = r ^. qRequestorId
    in atomically $ do
        orid ← readMVar (op ^. opRequestorId)
        unless (rid ≡ orid) $ do
            void $ tryTakeTMVar $ op ^. opResponses
            void $ tryTakeTMVar $ op ^. opRequests
            op ^. opRequestorIdInc

-- | Run a function with a 'Requestor' and clean up after.
withRequestor ∷ (MonadBase IO m) ⇒ Operator q r → (f → (Requestor q r) → m a) → m a
withRequestor f = op ^. requestor ≫= finally (unRequestor op) ∘ f

-- | Send a request and wait for a response.
request ∷ (MonadBase IO m) ⇒ Requestor q r → q → m (Maybe r)
request req q =
    let
        op = req ^. reqOperator
    in atomically $ do
        rid ← tryReadMVar (op ^. opRequestorId)
        unlessM ((≡ (Just $ req ^. qRequestorId)) <$> (tryReadMVar (op ^. opRequestorId))) $ do
            (unOperatorRequests <$> tryReadTMVar (op ^. opRequests)) ≫=
                flip writeTBChan ∘ Request (req ^. qRequestorId) q ≫
                tryReadTMVar (req ^. reqOperator ) ≫= readTBChan ≫= view rResponse
