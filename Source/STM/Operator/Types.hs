{-# Language TemplateHaskell #-}

{-|
Module:             STM.Operator.Types
Description:        Supporting types for the 'Request's and 'Response's.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module STM.Operator.Types (
    module STM.Base,
    RequestorId(..),
    RequestLength,
    Request,
    qRequestorId,
    qRequest,
    Response,
    rRequest,
    rResponse,
    OperatorRequests,
    operatorRequests,
    readRequest,
    OperatorResponses,
    operatorResponses,
    writeResponse,
    )where

import Lawless
import STM.Base
import Data.Word

-- * Queues, 'Request's, 'Response's

-- | The length of an 'Operator' queue, guaranteed to be positive.
newtype RequestLength = RequestLength Word64
    deriving (Eq, Ord, Show, Bounded, Enum, Num, Integral, Real)

-- | Current 'Requestor' identifier.
newtype RequestorId = RequestorId Word64
    deriving (Eq, Ord, Show, Bounded, Enum, Num, Integral, Real)

-- | A wrapper for a request that also has the RequestorId associated
-- with it.
data Request q r = Request {
    -- | The 'RequestorId' that generated this 'Request'.
    _qRequestorId ∷ RequestorId,
    _qRequest ∷ q
    }
makeLenses ''Request

-- | A wrapper for a response that also has the original 'Request'.
-- with it.
data Response q r = Response {
    -- | The 'Request' that generated this 'Response'.
    _rRequest ∷ Request q r,
    _rResponse ∷ r
    }
makeLenses ''Response

-- | The incoming requests from the current 'Requestor'.
newtype OperatorRequests q r = OperatorRequests { unOperatorRequests ∷ TBChan (Request q r) }

operatorRequests ∷ (Integral l) ⇒ l → STM (OperatorRequests q r)
operatorRequests l = OperatorRequests <$> newTBChan l

readRequest ∷ OperatorRequests q r → STM (Request q r)
readRequest = readTBChan ∘ unOperatorRequests

-- | The outgoing responses to requests from the current 'Requestor'.
newtype OperatorResponses q r = OperatorResponses { unOperatorResponses ∷ TBChan (Response q r) }

operatorResponses ∷ (Integral l) ⇒ l → STM (OperatorResponses q r)
operatorResponses l = OperatorResponses <$> newTBChan l

writeResponse ∷ OperatorResponses q r → (Response q r) → STM ()
writeResponse r s = writeTBChan (unOperatorResponses r) s
