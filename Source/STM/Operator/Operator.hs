{-# Language TemplateHaskell #-}

{-|
Module:             STM.Operator.Operator
Description:        An 'Operator' sends and receives messages from a 'Requestor'.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module STM.Operator.Operator (
    Operator,
    operator,
    ) where

import Lawless
import STM.Operator.Types

-- * An 'Operator' is a bidirectional channel. It can receive
-- requests from a 'Requestor', and will return the response over the
-- handle given to the 'Requestor'.

-- ** The 'Operator'

-- | The listener for commands. It can only have a single 'Requestor'
-- at any given time.
data Operator q r = Operator {
    -- | Queue length
    _opQueueLength ∷ RequestLength,

    -- | Identifier for the current Requestor. If this is different
    -- than before running a request, the request is no longer
    -- valid. We go back to waiting for the next request from the new
    -- Requestor.  and we dump it.
    _opRequestorId ∷ TVar RequestorId,

    -- | The 'OperatorRequests' receiving messages from the current 'Requestor'.
    _opRequests ∷ TMVar (OperatorRequests q r),

    -- | The 'OperatorResponses' for the current 'Requestor'. All
    -- responses from requests received via '_opRequests' are returned
    -- here.
    _opResponses ∷ TMVar (OperatorResponses q r)
    }

-- | Create a new 'Operator' with no 'Requestor'.
operator ∷ ∀ (m ∷ * → *) q r. (MonadBaseControl IO m) ⇒ RequestLength → m (Operator q r)
operator l =
    Operator l
    <$> newTVarIO 0
    ⊛ newEmptyTMVarIO
    ⊛ newEmptyTMVarIO

-- | Pass an 'Operator' to a 'Requestor' function after initializing the queues.
withOperator ∷ (MonadBaseControl IO m) ⇒ Operator q r → (Operator q r → m a) → m a
withOperator oper f = atomically $ do
    putTMVar (_opRequests oper) (operatorRequests (oper ^. opQueueLength))
    putTMVar (_opResponses oper) (operatorResponses (oper ^. opQueueLength))
    opRequestorIdInc oper

    finally (f oper) $ do
        takeTMVar (_opRequests oper)
        takeTMVar (_opResponses oper)
        opRequestorIdInc oper

-- ** Queue Management

-- | Returns the 'RequestLength' used for the 'OperatorRequests' and
-- 'OperatorResponses' queues.
opQueueLength ∷ Getter (Operator q r) RequestLength
opQueueLength = to _opQueueLength

opRequestorId ∷ Getter (Operator q r) (STM RequestorId)
opRequestorId = to $ readTVar ∘ _opRequestorId

-- | Increments the 'Operator' 'RequestorId'.
opRequestorIdInc ∷ Operator q r → STM (RequestorId)
opRequestorIdInc oper = modifyTVar (_opRequestorId oper) succ ≫ readTVar (_opRequestorId oper)

-- | Gets the current 'OperatorRequests' or waits until there is one.
opRequests ∷ Getter (Operator q r) (STM (OperatorRequests q r))
opRequests = to $ readTMVar ∘ _opRequests

-- | Gets the current 'OperatorResponses' or waits until there is one.
opResponses ∷ Getter (Operator q r) (STM (OperatorResponses q r))
opResponses = to $ readTMVar ∘ _opResponses

-- ** Handling 'Request's and sending 'Response's

-- | Check if a 'Request' is valid by checking that the 'Request'
-- 'qRequestorId' matches the current 'opRequestorId'. If it's
-- valid, returns 'Just' 'Request'. Otherwise returns 'Nothing'.
--
-- This happens in 'STM' so that it can be used both before processing
-- and after processing, since we can't process 'Request's in STM.
validRequest ∷ Operator q r → Request q r → STM (Maybe (Request q r))
validRequest oper req =
    ifM ((≡ (req ^. qRequestorId)) <$> oper ^. opRequestorId) (pure ∘ Just $ req) (pure Nothing)

-- | Receive a 'Request' from a 'Requestor', check if it's valid, and
-- if it is, return it.
readRequest ∷ (MonadBase IO m) ⇒ Operator q r → m (Maybe (Request q r))
readRequest op = atomically $ do
    (op ^. opRequests) ≫= readTBChan ∘ unOperatorRequests ≫= validRequest op

-- | Create and send a response if the 'RequestorId' matches the
-- current 'RequestorId' and the Requestor is still connected. If so,
-- return 'Just' 'Response'. Otherwise, return 'Nothing'.
sendResponse ∷ (MonadBase IO m) ⇒ Operator q r → Request q r → r → m (Maybe r)
sendResponse oper req resp =atomically $ do
    qch ← tryReadTBChan (_opRequests oper)
    rch ← tryReadTBChan (_opResponses oper)

    ifM (has _Just qch ∧ has _Just rch) svresp (pure Nothing)
        where
            svresp =
                validRequest oper req ≫= writeTBChan ∘ unOperatorResponses

-- | Reads a 'Request', calls @f@ with it, and only returns the
-- response if the 'RequestorId' hasn't changed.
runRequest ∷ (MonadBase IO m) ⇒ Operator q r → (q → m r) → m (Maybe r)
runRequest op f =
    readRequest op ≫= \q → return ∘ f (q ^. qRequest) ≫= sendResponse op q
