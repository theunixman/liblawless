module Machine (
    module Data.Machine.Concurrent,
    (⇝),
    (⇜),
    (↣),
    (↢),
    (⧻),
    (⫴)
    ) where

import Lawless hiding ((<~))
import Data.Machine.Concurrent
import Control.Monad.Trans.Control

infixr 9 ⇜
infixl 9 ⇝
infixr 7 ↢
infixl 7 ↣
infixr 6 ⧻
infixr 6 ⫴

(⇝) ∷ Monad m ⇒ MachineT m k b → ProcessT m b c → MachineT m k c
(⇝) = (~>)

(⇜) ∷ Monad m ⇒ ProcessT m b c → MachineT m k b → MachineT m k c
(⇜) = (<~)

(↣) ∷ MonadBaseControl IO m ⇒ MachineT m k b → ProcessT m b c → MachineT m k c
(↣) = (>~>)

(↢) ∷ MonadBaseControl IO m ⇒ ProcessT m b c → MachineT m k b → MachineT m k c
(↢) = (<~<)

(⧻) ∷ ∀ m a b c d. MonadBaseControl IO m ⇒ ProcessT m a b → ProcessT m c d → ProcessT m (Either a c) (Either b d)
(⧻) = splitSum

(⫴) ∷ ∀ m a b r. MonadBaseControl IO m ⇒ ProcessT m a r -> ProcessT m b r -> ProcessT m (Either a b) r
(⫴) = mergeSum
