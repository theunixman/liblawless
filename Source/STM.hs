{-# Language DataKinds, TemplateHaskell #-}

{-|
Module:             STM
Description:        Higher-level STM operations.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module STM (
    module STM.Base,
    module STM.Flag,
    module STM.Scatter
    ) where

import STM.Base
import STM.Flag
import STM.Scatter
