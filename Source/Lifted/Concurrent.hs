{-|
Module:             Lifted.Concurrent
Description:        Control.Concurrent.Lifted
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Lifted.Concurrent (
    module Control.Concurrent.Lifted
    ) where

import Control.Concurrent.Lifted
