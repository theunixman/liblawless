{-|
Module:             IO.Base
Description:        Lift operations to 'MonadBase' or 'MonadBaseControl'.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module IO.Base (
    module Control.Monad.Base,
        module Control.Monad.Trans.Control
    ) where

import Control.Monad.Base
import Control.Monad.Trans.Control
