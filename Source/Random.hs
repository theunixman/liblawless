{-|
Module:             Random
Description:        Lifted random value functions using the system random number generator.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Random (randomR, random) where

import Unicode
import System.IO (IO)
import IO.Base
import qualified System.Random as R

-- | Generate an endless range of random numbers between @[a, a]@
-- inclusive. (This is 'R.randomRIO' lifted.)
randomR ∷ (R.Random a, MonadBase IO m) ⇒ (a, a) → m a
randomR = liftBase ∘ R.randomRIO

-- | Generate an endless stream of values. (This is 'R.randomIO' lifted.)
random ∷ (R.Random a, MonadBase IO m) ⇒ m a
random = liftBase R.randomIO
