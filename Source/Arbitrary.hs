{-# OPTIONS_GHC -Wno-orphans#-}

{-|
Module:             Arbitrary
Description:        Provides Arbitrary instances of several types in this and other libraries.
Copyright:          © 2016 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <>
Stability:          experimental
Portability:        POSIX
-}

module Arbitrary where

import Lawless
import Test.QuickCheck (Arbitrary(..), listOf1, suchThat)
import Time
import Data.Time
import Data.Time.Clock()
import Data.Time.Calendar()
import Data.Text (Text)

instance Arbitrary Text where
    arbitrary = view packed <$> listOf1 arbitrary

instance Arbitrary Day where
    arbitrary = toEnum <$> arbitrary

instance Arbitrary DiffTime where
    arbitrary =
        fromRational <$> suchThat arbitrary (\t → t ≥ 0.0 ∧ t ≤ 86401.0)

instance Arbitrary UTCTime where
    arbitrary =
        UTCTime <$> arbitrary <*> arbitrary

instance Arbitrary Time where
    arbitrary = review _Time <$> arbitrary
