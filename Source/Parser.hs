module Parser
    (
     module Text.Parser.Char,
     module Text.Parser.Combinators
    ) where

import Text.Parser.Char
import Text.Parser.Combinators
