{-# LANGUAGE TemplateHaskell #-}
{-|
Module:             Types.ZName
Description:        Builder for a ZPool name.
Copyright:          © 2016 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <>
Stability:          experimental
Portability:        POSIX
-}

module Types.ZName where

import Lawless
import Aeson
import Text

data ZNDateTime = ISO8601 deriving (Show, Eq, Ord, Generic)
makePrisms ''ZNDateTime

instance FromJSON ZNDateTime where
    parseJSON = lawlessParseJSON

instance ToJSON ZNDateTime where
    toEncoding = lawlessToJSONEncoding

data ZNComponent =
    String Text |
    Datetime ZNDateTime
    deriving (Eq, Show, Ord, Generic)
makePrisms ''ZNComponent

instance FromJSON ZNComponent where
    parseJSON = lawlessParseJSON

instance ToJSON ZNComponent where
    toEncoding = lawlessToJSONEncoding

newtype ZNComponents = ZNComponents [ZNComponent]
    deriving (Eq, Show, Ord, Monoid, Generic)
makePrisms ''ZNComponents

instance FromJSON ZNComponents where
    parseJSON = lawlessParseJSON

instance ToJSON ZNComponents where
    toEncoding = lawlessToJSONEncoding

data ZName = ZName {
    _znSeparator ∷ Char,
    _znComponents ∷ ZNComponents
    } deriving (Eq, Show, Ord, Generic)
makePrisms ''ZName

instance FromJSON ZName where
    parseJSON = lawlessParseJSON

instance ToJSON ZName where
    toEncoding = lawlessToJSONEncoding
