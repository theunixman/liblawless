{-# LANGUAGE TemplateHaskell #-}
{-|
Module:             Types.ZPool
Description:        Description of a single ZPool
Copyright:          © 2016 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <>
Stability:          experimental
Portability:        POSIX
-}

module Types.ZPool where

import Lawless
import Aeson
import Types.ZName

data ZPool = ZPool {
    _zpName ∷ ZName
    } deriving (Eq, Show, Ord, Generic)
makePrisms ''ZPool

instance FromJSON ZPool where
    parseJSON = lawlessParseJSON

instance ToJSON ZPool where
    toEncoding = lawlessToJSONEncoding
