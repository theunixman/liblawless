-- | Build a zpool and a ZFS volume with reasonable defaults.

{-# Language TemplateHaskell, NoImplicitPrelude #-}

import Lawless
import System.Process
import Map

data ZFSOption = ZFSOption {
    _zfsOption ∷ Text,
    _zfsValue ∷ Text
    } deriving (Show, Eq, Ord)
makeLenses ''ZFSOptions

defaultOptions =
    foldMapOf folded singleton [
    ("atime", "off"),
    ("compression", "lz4"),
    ("normalization", "formD"),
    ("mountpoint", "none"),
    ("")
    ]
