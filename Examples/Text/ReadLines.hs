#!/usr/bin/env runhaskell

import Lawless
import Text
import Data.Text.IO
import Machine
import System.IO hiding (hGetLine)
import Bool
import Control.Monad.IO.Class
