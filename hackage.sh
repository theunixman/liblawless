#!/usr/bin/env zsh

set -e

readonly package=$(basename $PWD)
readonly version_pfx="v"
readonly version=$(awk '$1 == "version:" {print $2}' "${package}.cabal")
readonly version_tag="${version_pfx}${version}"

if ! git tag --sort=v:refname | grep "${version_tag}" &> /dev/null; then
    echo Tag "${version_tag}" not found. Please tag. >&2
    exit 1
fi

cabal sdist
cabal haddock --for-hackage
cabal upload dist/"${package}-${version}.tar.gz"
cabal upload --documentation dist/"${package}-${version}-docs.tar.gz"
