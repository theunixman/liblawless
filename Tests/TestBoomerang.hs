{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -Wno-orphans #-}

{-|
Module:             TestBoomerang
Description:        Test the Boomerang module of liblawless.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module TestBoomerang where

import Lawless
import Arbitrary

import Test.Framework
import Test.Framework.TH
import Test.Framework.Providers.QuickCheck2 (testProperty)
import Test.QuickCheck

data PipeDelimited = PipeDelimited [Text] deriving (Eq, Show, Ord)
instance Arbitrary PipeDelimited where
    arbitrary =
        let
            w = (suchThat arbitrary $ allOf each (≢ '|'))
        in
            PipeDelimited <$> listOf1 w

properties ∷ Test
properties = $(testGroupGenerator)
