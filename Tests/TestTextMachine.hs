{-# LANGUAGE TemplateHaskell, OverloadedStrings #-}
{-# OPTIONS_GHC -Wno-orphans #-}

{-|
Module:             TestTextMachine
Description:        Test reading lines from text files.
Copyright:          © 2016 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module TestTextMachine where

import Lawless
import Text hiding (text)

import Arbitrary()
import Test.Framework
import Test.Framework.TH
import Test.Framework.Providers.QuickCheck2 (testProperty)
import Test.QuickCheck
import Test.QuickCheck.Monadic
import Line

default (Text)

prop_CheckTextMachine ∷ [Line] → Property
prop_CheckTextMachine lns = monadicIO $ do
    m ← run $ readWriteLines lns
    assert (lns == m)

properties ∷ Test
properties = $(testGroupGenerator)
