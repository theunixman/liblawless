{-|
Module:             Line
Description:        Arbitrary Text Line.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Line (Line(..), readWriteLines) where

import Lawless hiding (mapping)
import Textual
import Arbitrary()
import Test.QuickCheck
import Text hiding (text)
import Text.IO
import IO hiding (putStrLn)
import Machine
import Path

newtype Line = Line Text deriving (Eq, Ord, Show, Printable)
instance Arbitrary Line where
    arbitrary = Line <$> suchThat arbitrary (allOf each (≥ ' '))

readWriteLines ∷
    (Traversable f, MonadMask m, MonadIO m) ⇒
    f Line → m [Line]
readWriteLines lns = textTemporaryFile (absDir "/tmp") (relFile "testTemp") $ \tf → do
    let h = tf ^. tfHandle
    withCurrentPosition h $ \i →
        runT_ $ supply lns (mapping $ \(Line t) → t) ⇝ writeLines i
    runT $ readLines h ⇝ mapping Line
