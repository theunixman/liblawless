{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module TestTime where

import Test.Framework
import Test.Framework.TH
import Test.Framework.Providers.QuickCheck2 (testProperty)
import Test.QuickCheck
import qualified Data.ByteString.Lazy as L
import Data.Time.Clock
import Data.Binary
import Lawless
import Time
import Arbitrary()

prop_PrismToTime :: UTCTime -> Property
prop_PrismToTime (ut :: UTCTime) =
  ut ^.re _Time ^. day === utctDay ut .&&.
  ut ^.re _Time ^. time === utctDayTime ut

prop_PrismFromTime :: Time -> Property
prop_PrismFromTime (t :: Time) =
  utctDay (t ^. _Time) === t ^. day .&&.
  utctDayTime (t ^. _Time) === t ^. time

prop_Serialize :: Time -> Property
prop_Serialize (t :: Time) =
  let
    sz = L.toStrict $ encode t
    de = decode $ L.fromStrict sz
  in
    de === t

properties ∷ Test
properties = $(testGroupGenerator)
