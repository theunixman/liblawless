module Main where

import Lawless
import Test.Framework (defaultMain)

import qualified TestTime as TT
import qualified TestAeson as TJ
import qualified TestTemporary as TM
import qualified TestAesonEncoding as TE
import qualified TestTextMachine as TX

main :: IO ()
main = defaultMain [
    TT.properties,
    TJ.properties,
    TM.properties,
    TE.properties,
    TX.properties
    ]
