{-|
Module:             PipeDelimited
Description:        Data type for testing Boomerangs.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

{-# Language TemplateHaskell #-}

module PipeDelimited where

import Lawless hiding ((∘), (<>))
import Arbitrary
import Boomerang
import Test.QuickCheck

newtype PipeDelimited = PipeDelimited [Text] deriving (Eq, Show, Ord)
makePrisms ''PipeDelimited
makeBoomerangs ''PipeDelimited

-- pipeDelimited =
--     (
--         rPipeDelimited ∘ chainr (satisfy (≢ '|')) "|"
--     )

-- instance Arbitrary PipeDelimited where
--     arbitrary =
--         let
--             w = (suchThat arbitrary $ allOf each (≢ '|'))
--         in
--             PipeDelimited <$> listOf w
