#!/usr/bin/env zsh

set -e

readonly test_options=(
    --maximum-generated-tests=500
    --maximum-unsuitable-generated-tests=100000
    --maximum-test-depth=100000
    --threads=4
    +RTS
    -N
)

readonly test_path="./dist/build/test-liblawless/test-liblawless"

[ -z "NOCLEAN" ] && cabal clean
cabal build -j4
LD_LIBRARY_PATH=./dist/build:${LD_LIBRARY_PATH+:${LD_LIBRARY_PATH}} \
               "${test_path}" "${test_options[@]}"
